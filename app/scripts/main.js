$(document).ready(function(){
    // tab --- [[
    $('.featuredContent__tab li:first-child').addClass('active');
    $('.tab-item').hide();
    $('.tab-item:first').show();

    $('.featuredContent__tab li').click(function(){
    $('.featuredContent__tab li').removeClass('active');
    $(this).addClass('active');
    $('.tab-item').hide();
    
    var activeLink = $(this).find('a').attr('href');
    $(activeLink).fadeIn();
    return false;
    });   
    // tab --- ]]

    // menu [[
    $('.buttonInMobile img').click(function() {
        $('.mobileNav').slideToggle(300);
    })    
    // menu ]]
     
})
